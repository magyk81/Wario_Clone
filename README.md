# Assignment 1 - Mario Clone

<h4>CS 491 / IFDM 491 - Spring 2018</h4>

<br><b>About:</b><br>
  <br>The player dies when his health goes to zero.
  <br>Try to kill as many enemies as possible to get a high score.
  <br>Kill tumbleweeds and snakes by landing on top of them.
  <br>Cacti can't be killed, but they still hurt the player.<br>
  
  This game was not developed using assets or scripts form the 2D platformer demo.<br>
  All scripts and sprites were made by the team members without using the Asset Store.<br>

<br><b>Team Members:</b><br>
  <br>Michael Roybal
  <br>Robin Campos<br>
  
<br><b>Controls:</b><br>
  <br>A - Move left
  <br>D - Move right
  <br>S - Crouch
  <br>Space - Jump<br>
  
<br><b>Credits:</b><br>
  <p>The font that was used for the score is "Western" by James L. DeVriese (www.dafont.com)</p>