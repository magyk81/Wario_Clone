﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CactusScript : MonoBehaviour
{
    public GameObject player;
    public Sprite[] sprites;

    private int framesPassed = 0;
    private float scaleX, scaleY;
    private int facingDir = 1;
    private int currentSprite = 0;

	// Use this for initialization
	void Start ()
    {
        scaleX = transform.localScale.x;
        scaleY = transform.localScale.y;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (framesPassed == 0)
        {
            if (currentSprite == 0) currentSprite = 1;
            else currentSprite = 0;
        }
        GetComponent<Renderer>().material.mainTexture
            = sprites[currentSprite].texture;

        if (player.GetComponent<Renderer>().transform.position.x > transform.position.x)
        {
            faceDirection(true);
        }
        else faceDirection(false);

        if (framesPassed < 59) framesPassed++;
        else framesPassed = 0;
    }

    private void faceDirection()
    {
        GetComponent<Renderer>().transform.localScale
            = new Vector2(facingDir * scaleX, scaleY);
    }

    private void faceDirection(bool faceLeft)
    {
        if (faceLeft) facingDir = -1;
        else facingDir = 1;

        faceDirection();
    }
}
