﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumbleweedScript : MonoBehaviour {

    public int maxSpeed;

    private Rigidbody2D rigidBody;

    private int framesPassed = 0;

	// Use this for initialization
	void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (tag == "Corpse") return;

        if (framesPassed == 0)
        {
            if (rigidBody.velocity.x > -maxSpeed)
            {
                rigidBody.AddForce(new Vector2(-200, 0));
            }
            if (rigidBody.velocity.x < -maxSpeed)
            {
                rigidBody.AddForce(new Vector2(200, 0));
            }
            if (rigidBody.velocity.y > -maxSpeed)
            {
                rigidBody.AddForce(new Vector2(0, -200));
            }
            if (rigidBody.velocity.y > maxSpeed / 2)
            {
                rigidBody.AddForce(new Vector2(0, -500));
            }
        }

        if (framesPassed < 59) framesPassed++;
        else framesPassed = 0;
    }

    void Update()
    {
        transform.Rotate(Vector3.forward, -rigidBody.velocity.x / 10);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Hazard")
        {
            rigidBody.velocity
                = new Vector2(rigidBody.velocity.x / 2,
                rigidBody.velocity.y / 2);
        }
    }
}
