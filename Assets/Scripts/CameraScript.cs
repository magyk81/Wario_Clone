﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject player;
    public GameObject score;

    private GameObject topMessage;

    private float zPos;

    // Use this for initialization
    void Start()
    {
        zPos = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.tag != "Corpse") transform.position
            = new Vector3(player.transform.position.x,
            player.transform.position.y, zPos);

        int orthoSize = (int) GetComponent<Camera>().orthographicSize;

        score.transform.position
            = new Vector3(transform.position.x - orthoSize * 2,
            transform.position.y + orthoSize * 0.9f, -1);
    }

    public void gameOverScreen()
    {
        topMessage = Instantiate(score);
        TextMesh textMesh = topMessage.GetComponent<TextMesh>();
        textMesh.alignment = TextAlignment.Center;
        textMesh.anchor = TextAnchor.MiddleCenter;
        textMesh.fontSize = 80;
        textMesh.text = "Game Over";
        topMessage.transform.position
            = new Vector3(transform.position.x, transform.position.y,
            score.transform.position.z);
    }

    /* Font for score: "Western" by James L. DeVriese from www.dafont.com */
}