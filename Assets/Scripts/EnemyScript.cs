﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public GameObject player;

    private bool dying = false;
    private bool gameOver = false;

    // Use this for initialization
    void Start ()
    {
        if (tag == "Player") return;

        if (GetComponent<CircleCollider2D>() != null)
        {
            Physics2D.IgnoreCollision(GetComponent<CircleCollider2D>(),
                player.GetComponent<BoxCollider2D>());
        }
        else
        {
            Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(),
                player.GetComponent<BoxCollider2D>());
        }
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (dying)
        {
            ParticleSystem particleSystem = GetComponent<ParticleSystem>();
            if (particleSystem != null) particleSystem.Emit(1);
            if (transform.localScale.y < 0.5)
            {
                gameObject.SetActive(false);
            }
            transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y - 0.2f);
        }

        if (transform.position.y < -700) die();
	}

    public void die()
    {
        tag = "Corpse";
        dying = true;

        if (gameObject == player && !gameOver)
        {
            gameOver = true;
            player.GetComponent<WarioScript>().gameOver();
            Destroy(player.GetComponent<BoxCollider2D>());
        }
    }
}
