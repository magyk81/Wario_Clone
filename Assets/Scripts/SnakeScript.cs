﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeScript : MonoBehaviour {

    public Sprite[] sprites;

    public int moveSpeed;

    private Rigidbody2D rigidBody;

    private float scaleX, scaleY;
    private float lastPositionX;

    private int framesPassed = 0;

    // Use this for initialization
    void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        scaleX = transform.localScale.x;
        scaleY = transform.localScale.y;
        lastPositionX = transform.position.x;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (tag == "Corpse") return;

        if (framesPassed == 0 && checkIfStuck())
        {
            facingDir *= (-1);
            faceDirection();
        }

        if (framesPassed % 7 == 0)
        {
            nextSprite();
        }

        if (framesPassed < 15) framesPassed++;
        else framesPassed = 0;
    }

    private int facingDir = -1;
    private void faceDirection()
    {
        GetComponent<Renderer>().transform.localScale
            = new Vector2(facingDir * scaleX, scaleY);
    }

    private bool checkIfStuck()
    {
        if (Mathf.Abs(lastPositionX - transform.position.x) < 10)
        {
            return true;
        }
        lastPositionX = transform.position.x;
        return false;
    }

    private int spriteIndex = 0;
    private void nextSprite()
    {
        if (spriteIndex == 0)
        {
            spriteIndex = 1;

            float newVelX = rigidBody.velocity.x + (moveSpeed * -facingDir);
            float newVelY = rigidBody.velocity.y;
            rigidBody.velocity = new Vector2(newVelX , newVelY);
        }
        else spriteIndex = 0;
        GetComponent<Renderer>().material.mainTexture = sprites[spriteIndex].texture;
        faceDirection();
    }
}
