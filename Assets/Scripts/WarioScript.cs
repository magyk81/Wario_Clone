﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarioScript : MonoBehaviour
{
    public Sprite[] sprites_idle;
    public Sprite[] sprites_run;
    public Sprite sprite_jump;

    public GameObject scoreGameObject;

    public int maxRunningSpeed;
    public int runningForce;
    public int airborneForce;
    public int jumpingSpeed;

    private Rigidbody2D rigidBody;

    private int framesPassed = 0;
    private float scaleX, scaleY;
    private int spriteIndex = 0;

    private int grounded = 0;
    private float crouch = 1;

    private int score = 0;
    public int health = 3;
    private bool invulnerable = false;

	// Use this for initialization
	void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        scaleX = transform.localScale.x;
        scaleY = transform.localScale.y;

        GetComponent<Renderer>().material.mainTexture = sprite_jump.texture;

        updateText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (tag == "Corpse") return;

        if (Input.GetKey(KeyCode.A) && rigidBody.velocity.x > -maxRunningSpeed && crouch == 1)
        {
            if (grounded < 2) rigidBody.AddForce(new Vector2(-runningForce, 0));
            else rigidBody.AddForce(new Vector2(-airborneForce * crouch / 2, 0));
            faceDirection(true);
        }
        if (Input.GetKey(KeyCode.D) && rigidBody.velocity.x < maxRunningSpeed && crouch == 1)
        {
            if (grounded < 2) rigidBody.AddForce(new Vector2(runningForce, 0));
            else rigidBody.AddForce(new Vector2(airborneForce * crouch, 0));
            faceDirection(false);
        }
        if (Input.GetKey(KeyCode.Space) && grounded < 2 && crouch == 1)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, rigidBody.velocity.y + jumpingSpeed);
        }

        if (grounded == 2)
        {
            GetComponent<Renderer>().material.mainTexture = sprite_jump.texture;
            faceDirection();
        }
        else if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.Space)
            && !Input.GetKey(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.S))
            {
                crouch = 0.5f;
            }
            else crouch = 1;

            if (spriteIndex > 1)
            {
                spriteIndex = Random.Range(0, 2);
            }
            GetComponent<Renderer>().material.mainTexture = sprites_idle[spriteIndex].texture;
            faceDirection();
        }
        else if (framesPassed % 5 == 0)
        {
            nextSprite();
        }

        if (grounded < 2) grounded++;

        if (framesPassed % 60 == 0) invulnerable = false;

        if (framesPassed < 59) framesPassed++;
        else framesPassed = 0;

        if (health < 1) GetComponent<EnemyScript>().die();
    }

    public void gameOver()
    {
        GameObject.FindGameObjectWithTag("MainCamera")
            .GetComponent<CameraScript>().gameOverScreen();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject other = collider.gameObject;
        Transform otherTrans = other.GetComponent<Renderer>().transform;

        if (other.tag == "Environment")
        {

            if (otherTrans.position.y + (otherTrans.localScale.y / 2)
                < transform.position.y - (transform.localScale.y / 2)
                && rigidBody.velocity.y <= 0)
            {
                grounded = 0;
            }
        }

        else if (other.tag == "Hazard")
            getHurt(other.GetComponent<Renderer>().transform.position.x);

        else if (other.tag == "Enemy")
        {
            if (otherTrans.position.y
                > transform.position.y - (transform.localScale.y / 2))
            {
                getHurt(otherTrans.position.x);
                return;
            }

            float otherXleft = otherTrans.position.x - (Mathf.Abs(otherTrans.localScale.x / 2));
            float otherXright = otherXleft + Mathf.Abs(otherTrans.localScale.x);

            float thisXleft = transform.position.x - (Mathf.Abs(transform.localScale.x / 2));
            float thisXright = thisXleft + Mathf.Abs(transform.localScale.x);

            if (thisXleft < otherXright && thisXright > otherXleft)
            {
                collider.GetComponent<EnemyScript>().die();

                score++;
                updateText();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        OnTriggerEnter2D(collider);
    }

    private void getHurt(float otherX)
    {
        if (invulnerable) return;

        health--;
        updateText();

        invulnerable = true;

        float thisX = GetComponent<Renderer>().transform.position.x;

        int direction = 1;
        if (thisX < otherX) direction = -1;

        rigidBody.velocity = new Vector2(100 * direction, 0);
    }

    private int facingDir = 1;
    private void faceDirection()
    {
        GetComponent<Renderer>().transform.localScale
            = new Vector2(facingDir * scaleX, scaleY * crouch);
    }

    private void faceDirection(bool faceLeft)
    {
        if (faceLeft) facingDir = -1;
        else facingDir = 1;

        faceDirection();
    }

    private void nextSprite()
    {
        if (spriteIndex == sprites_run.Length - 1) spriteIndex = 0;
        else spriteIndex++;
        GetComponent<Renderer>().material.mainTexture = sprites_run[spriteIndex].texture;
        faceDirection();
    }

    private void updateText()
    {
        scoreGameObject.GetComponent<TextMesh>().text = "Score: " + score + "\nHealth: " + health;
    }
}
